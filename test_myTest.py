import unittest
import test
import teet

class TestCiCd(unittest.TestCase):

    def test_add_int(self):
        result = test.add_int(1,4)
        self.assertEqual(result,5)
    def test_sub_int(self):
        result = test.sub_int(4,1)
        self.assertEqual(result,3)
    def test_mul_int(self):
        result = test.mul_int(2,4)
        self.assertEqual(result,8)
    def test_div_int(self):
        result = test.div_int(4,4)
        self.assertEqual(result,1)
    def test_garbage(self):
        result = teet.garbage()
        self.assertEqual(result,0)
        
if __name__ == '__main__':
    unittest.main(verbosity = 2)